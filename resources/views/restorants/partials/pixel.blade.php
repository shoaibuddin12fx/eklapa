<h6 class="heading-small text-muted mb-4">{{ __('Facebook Pixel & Google Analytics') }}</h6>
<div class="pl-lg-4">

    <form id="restorant-form" method="post" action="{{ route('admin.restaurant.pixelupdate', $restorant) }}"
        autocomplete="off" enctype="multipart/form-data">
        @csrf
        <div class="form-group{{ $errors->has('pixel_id') ? ' has-danger' : '' }}">
            <label class="form-control-label" for="pixel_id">{{ __('Pixel Id') }}</label>
            <input type="text" name="pixel_id" id="pixel_id" class="form-control form-control-alternative"
                placeholder="{{ __('Pixel Id') }}" value="{{ old('name', $restorant->pixel->pixel_id) }}">
        </div>
        <div class="form-group{{ $errors->has('pixel_domain') ? ' has-danger' : '' }}">
            <label class="form-control-label" for="pixel_domain">{{ __('Pixel Domain') }}</label>
            <input type="text" name="pixel_domain" id="pixel_domain" class="form-control form-control-alternative"
                placeholder="{{ __('Pixel Domain') }}" value="{{ old('name', $restorant->pixel->pixel_domain) }}">
        </div>
        <div class="form-group{{ $errors->has('analytics_id') ? ' has-danger' : '' }}">
            <label class="form-control-label" for="analytics_id">{{ __('Analytics Id') }}</label>
            <input type="text" name="analytics_id" id="analytics_id" class="form-control form-control-alternative"
                placeholder="{{ __('Analytics Id') }}" value="{{ old('name', $restorant->pixel->analytics_id) }}">
        </div>



        <div class="text-center">
            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
        </div>

    </form>

</div>
