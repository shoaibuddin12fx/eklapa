<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PixelAccount extends Model
{
    use HasFactory;

    protected $table = 'pixel_accounts';
    protected $guarded = [];
}
