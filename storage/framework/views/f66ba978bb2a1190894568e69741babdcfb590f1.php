<h6 class="heading-small text-muted mb-4"><?php echo e(__('Facebook Pixel & Google Analytics')); ?></h6>
<div class="pl-lg-4">

    <form id="restorant-form" method="post" action="<?php echo e(route('admin.restaurant.pixelupdate', $restorant)); ?>"
        autocomplete="off" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <div class="form-group<?php echo e($errors->has('pixel_id') ? ' has-danger' : ''); ?>">
            <label class="form-control-label" for="pixel_id"><?php echo e(__('Pixel Id')); ?></label>
            <input type="text" name="pixel_id" id="pixel_id" class="form-control form-control-alternative"
                placeholder="<?php echo e(__('Pixel Id')); ?>" value="<?php echo e(old('name', $restorant->pixel->pixel_id)); ?>">
        </div>
        <div class="form-group<?php echo e($errors->has('pixel_domain') ? ' has-danger' : ''); ?>">
            <label class="form-control-label" for="pixel_domain"><?php echo e(__('Pixel Domain')); ?></label>
            <input type="text" name="pixel_domain" id="pixel_domain" class="form-control form-control-alternative"
                placeholder="<?php echo e(__('Pixel Domain')); ?>" value="<?php echo e(old('name', $restorant->pixel->pixel_domain)); ?>">
        </div>
        <div class="form-group<?php echo e($errors->has('analytics_id') ? ' has-danger' : ''); ?>">
            <label class="form-control-label" for="analytics_id"><?php echo e(__('Analytics Id')); ?></label>
            <input type="text" name="analytics_id" id="analytics_id" class="form-control form-control-alternative"
                placeholder="<?php echo e(__('Analytics Id')); ?>" value="<?php echo e(old('name', $restorant->pixel->analytics_id)); ?>">
        </div>



        <div class="text-center">
            <button type="submit" class="btn btn-success mt-4"><?php echo e(__('Save')); ?></button>
        </div>

    </form>

</div>
<?php /**PATH C:\xampp\htdocs\eklapa\resources\views/restorants/partials/pixel.blade.php ENDPATH**/ ?>